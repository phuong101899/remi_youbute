import { render, screen } from '@testing-library/react';
import {MemoryRouter} from "react-router-dom";
import App from "../../App";


test('Renders Home component', () => {
    render(
        <MemoryRouter initialEntries={['/share']} >
            <App />
        </MemoryRouter>
    )
    const linkElement = screen.getByText(/You must sign in to access this page/i);
    expect(linkElement).toBeInTheDocument();
});
