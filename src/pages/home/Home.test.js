import { render, screen } from '@testing-library/react';
import {MemoryRouter} from "react-router-dom";
import App from "../../App";


test('Renders Home component', () => {
    render(
        <MemoryRouter initialEntries={['/']} >
            <App />
        </MemoryRouter>
    )
    const linkElement = screen.getByText(/Funny movies/i);
    expect(linkElement).toBeInTheDocument();
});
